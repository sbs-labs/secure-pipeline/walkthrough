# Helm V2 Tiller
Not actually a recommended way to install tiller or use helm with production clusters.

```bash
# create the service account and role binding for tiller
kubectl create sa tiller --namespace kube-system
kubectl create clusterrolebinding --serviceaccount kube-system:tiller --clusterrole cluster-admin tiller-crb

# initing helm with tiller service account
helm init --service-account tiller
```